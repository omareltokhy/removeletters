﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RemoveTheWord
{
    class Program
    {
        static void Main(string[] args)
        {
            char[] arr1 = new char[] { 's','t', 'r', 'i', 'n', 'g', 'w' };
            string str1 = "string";
            foreach(char i in RemoveLetters(arr1, str1)) { Console.Write(i + ","); }
            Console.WriteLine();

            char[] arr2 = new char[] { 'b', 'b', 'l', 'l', 'g', 'n', 'o', 'a', 'w' };
            string str2 = "balloon";
            foreach (char i in RemoveLetters(arr2, str2)) { Console.Write(i + ","); }
            Console.WriteLine();

            char[] arr3 = new char[] { 'a', 'n', 'r', 'y', 'o', 'w' };
            string str3 = "norway";
            foreach (char i in RemoveLetters(arr3, str3)) { Console.Write(i + ","); }
            Console.WriteLine();

            char[] arr4 = new char[] { 't', 't', 'e', 's', 't', 'u' };
            string str4 = "testing";
            foreach (char i in RemoveLetters(arr4, str4)) { Console.Write(i + ","); }
            Console.WriteLine();
        }
        public static Array RemoveLetters(char[] arr, string str)
        {
            str.ToCharArray();
            var arrList = arr.ToList();

            for(int i = 0; i < str.Length; i++)
            {
                for(int j = 0; j < arr.Length; j++)
                {
                    if (str[i] == arr[j])
                    {
                        arrList.Remove(arr[j]);
                        break;
                    }
                }
            }

            return arrList.ToArray();
        }
    }
}
